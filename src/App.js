import { Route, Switch } from "react-router-dom";

import AllMeetupsPage from "./pages/AllMeetups";
import NewMeetupsPage from "./pages/NewMeetup";
import FavouriteMeetupsPage from "./pages/FavouriteMeetup";
import Layout from "./components/layout/Layout";

function App() {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact>
          <AllMeetupsPage />
        </Route>
        <Route path="/new-meet">
          <NewMeetupsPage />
        </Route>
        <Route path="/favourite">
          <FavouriteMeetupsPage />
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
