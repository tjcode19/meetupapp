import { Link } from "react-router-dom";
import { useContext } from "react";

import FavouriteContext from "../../store/favourite-context";

import classes from "./MainNav.module.css";
const MainNav = () => {
  const favCtx = useContext(FavouriteContext);

  return (
    <header className={classes.header}>
      <div className={classes.logo}>Coded Meetup</div>
      <ul>
        <li>
          <Link to="/">All Meetups</Link>
        </li>
        <li>
          <Link to="/new-meet">New Meetups</Link>
        </li>
        <li>
          <Link to="/favourite">
            Favourite Meetups{" "}
            <span className={classes.badge}>{favCtx.totalFavourite}</span>
          </Link>
        </li>
      </ul>
    </header>
  );
};

export default MainNav;
