import { useContext } from "react";
import classes from "./MeetupItem.module.css";
import Card from "../ui/Card";

import FavouriteContext from "../../store/favourite-context";

const MeetupItem = (props) => {
  const favouriteCtx = useContext(FavouriteContext);

  const isItemFav = favouriteCtx.isItemFavourite(props.data.id);

  const toggleFavButton = () => {
      console.log(props.data.id)
      console.log(isItemFav)

    if (isItemFav) {
      favouriteCtx.removeFavourite(props.data.id);
    } else {
      favouriteCtx.addFavourite({
          id:props.data.id,
          title: props.data.title,
          image: props.data.image,
          description: props.data.description,
          address: props.data.address
      });
    }
  };

  return (
    <li className={classes.item}>
      <Card>
        <div className={classes.image}>
          <img src={props.data.image} alt=" " />
        </div>
        <div className={classes.content}>
          <h1>{props.data.title}</h1>
          <address>{props.data.address}</address>
          <p>{props.data.description}</p>
        </div>
        <div className={classes.actions}>
          <button onClick={toggleFavButton}>
            {isItemFav ? "Remove from Favourite" : "To Favourite"}
          </button>
        </div>
      </Card>
    </li>
  );
};

export default MeetupItem;
