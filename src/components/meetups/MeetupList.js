import MeetupItem from "./MeetupItem";
import classes from "./MeetupList.module.css";

const MeetupList = (props) => {
  return (
    <ul className={classes.list}>
      {props.data.map((items) => {
        return <MeetupItem key={items.id} data={items} />;
      })}
    </ul>
  );
};
export default MeetupList;
