import { createContext, useState } from "react";

const FavouriteContext = createContext({
  favourites: [],
  totalFavourite: 0,
  addFavourite: (favouriteMeetups) => {},
  removeFavourite: (meetupId) => {},
  isItemFavourite: (meetupId) => {},
});

export function FavouriteContextProvider(props) {
  const [userFavourite, setUserFavourite] = useState([]);

  const addFavouriteHandler = (favouriteMeetups) => {
    setUserFavourite((prevUserFav) => {
      return prevUserFav.concat(favouriteMeetups);
    });
  };
  const removeFavouriteHandler = (meetupId) => {
    setUserFavourite((prevUserfav) => {
      return prevUserfav.filter((meetup) => meetup.id !== meetupId);
    });
  };
  const isItemFavouriteHandler = (meetupId) => {
    return userFavourite.some(meetup => meetup.id === meetupId);
  };

  const context = {
    favourites: userFavourite,
    totalFavourite: userFavourite.length,
    addFavourite: addFavouriteHandler,
    removeFavourite: removeFavouriteHandler,
    isItemFavourite: isItemFavouriteHandler,
  };

  return (
    <FavouriteContext.Provider value={context}>
      {props.children}
    </FavouriteContext.Provider>
  );
}

export default FavouriteContext;
