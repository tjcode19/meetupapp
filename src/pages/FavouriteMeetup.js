import { useContext } from "react";

import MeetupList from "../components/meetups/MeetupList";
import FavouriteContext from "../store/favourite-context";

const FavouriteMeetupsPage = () => {
  const favouriteMeetupsCtx = useContext(FavouriteContext);

  const data =
    favouriteMeetupsCtx.totalFavourite > 0 ? (
      <MeetupList data={favouriteMeetupsCtx.favourites} />
    ) : (
      <p>No Favourite Meetup</p>
    );

  return (
    <section>
      <h2>My Favourites</h2>
      {data}
    </section>
  );
};

export default FavouriteMeetupsPage;
