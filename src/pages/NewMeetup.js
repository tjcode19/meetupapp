import { useHistory } from "react-router-dom";
import NewMeetupForm from "../components/meetups/NewMeetupForm";

const NewMeetupsPage = () => {
  const history = useHistory();
  const addNewMeetupHandler = (meetData) => {
    fetch("https://meetup-app-94502-default-rtdb.firebaseio.com/meetups.json", {
      method: "POST",
      body: JSON.stringify(meetData),
      headers: {
        "Content-Type": "application/json",
      },
    }).then(() => {
      history.replace("/");
    });
  };
  return (
    <section>
      <h2>Add New Meetup</h2>
      <NewMeetupForm onAddNewMeetup={addNewMeetupHandler} />
    </section>
  );
};

export default NewMeetupsPage;
